//- Class: CubitEvent
//- Description: Class describing an event. This base class is sufficient
//-              to describe basic unary events, like destroying a model 
//-              entity. Derived classes describe events requiring extra
//-              data, such as merging two model entities.
//- Owner:  

#ifndef CUBITEVENT_HPP
#define CUBITEVENT_HPP

#include "CGMUtilConfigure.h"

class CUBIT_UTIL_EXPORT CubitEvent
{
public:
  virtual ~CubitEvent();
};


#endif

