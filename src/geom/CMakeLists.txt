project(cgm_geom CXX)

include(CGMMacros)

set(geom_srcs
  AnalyticGeometryTool.cpp
  AutoMidsurfaceTool.cpp
  BasicTopologyEntity.cpp
  Body.cpp
  BodySM.cpp
  BoundingBoxTool.cpp
  BridgeManager.cpp
  CAActuateSet.cpp
  CADeferredAttrib.cpp
  CAEntityColor.cpp
  CAEntityId.cpp
  CAEntityName.cpp
  CAEntitySense.cpp
  CAEntityTol.cpp
  CAGroup.cpp
  CAMergePartner.cpp
  CAMergeStatus.cpp
  CASourceFeature.cpp
  CAUniqueId.cpp
  CGMApp.cpp
  CGMEngineDynamicLoader.cpp
  CGMHistory.cpp
  Chain.cpp
  CoEdge.cpp
  CoEdgeSM.cpp
  CoFace.cpp
  CoVertex.cpp
  CoVolume.cpp
  CollectionEntity.cpp
  CubitAttrib.cpp
  CubitAttribManager.cpp
  CubitAttribUser.cpp
  CubitPolygon.cpp
  CubitSimpleAttrib.cpp
  Curve.cpp
  CurveOverlapFacet.cpp
  CurveSM.cpp
  CylinderEvaluator.cpp
  DAG.cpp
  DagDrawingTool.cpp
  GSaveOpen.cpp
  GeomDataObserver.cpp
  GeomMeasureTool.cpp
  GeometryEntity.cpp
  GeometryEvent.cpp
  GeometryFeatureEngine.cpp
  GeometryFeatureTool.cpp
  GeometryHealerEngine.cpp
  GeometryHealerTool.cpp
  GeometryModifyEngine.cpp
  GeometryModifyTool.cpp
  GeometryQueryEngine.cpp
  GeometryQueryTool.cpp
  GeometryUtil.cpp
  GfxPreview.cpp
  GroupingEntity.cpp
  LocalToleranceTool.cpp
  Loop.cpp
  LoopSM.cpp
  Lump.cpp
  LumpSM.cpp
  MedialTool2D.cpp
  MedialTool3D.cpp
  MergeTool.cpp
  MergeToolAssistant.cpp
  MidPlaneTool.cpp
  #ModelEntity.cpp
  ModelQueryEngine.cpp
  OffsetSplitTool.cpp
  OldUnmergeCode.cpp
  Point.cpp
  PointSM.cpp
  RefCollection.cpp
  RefEdge.cpp
  RefEntity.cpp
  RefEntityFactory.cpp
  RefEntityName.cpp
  RefFace.cpp
  RefGroup.cpp
  RefVertex.cpp
  RefVolume.cpp
  SenseEntity.cpp
  Shell.cpp
  ShellSM.cpp
  SphereEvaluator.cpp
  SplitSurfaceTool.cpp
  SurfParamTool.cpp
  Surface.cpp
  SurfaceOverlapFacet.cpp
  SurfaceOverlapTool.cpp
  SurfaceSM.cpp
  TBOwner.cpp
  TBOwnerSet.cpp
  TDCAGE.cpp
  TDSourceFeature.cpp
  TDSplitSurface.cpp
  TDSurfaceOverlap.cpp
  TDUniqueId.cpp
  TopologyBridge.cpp
  TopologyEntity.cpp)

set(extra_geom_srcs
  AllocMemManagersGeom.cpp
  CubitCompat.cpp
 )

# Create a list of header-only files:
set(geom_headers
  cgm/CADefines.hpp
  cgm/CubitEvaluator.hpp
  cgm/CubitCompat.h
  cgm/CubitCompat.hpp
  cgm/DagType.hpp
  cgm/GeomPoint.hpp
  cgm/GeomSeg.hpp
  cgm/GeometryEvent.hpp
  cgm/GeometryFeatureEngine.hpp
  cgm/GeometryHealerEngine.hpp
  cgm/IntermediateGeomEngine.hpp
  cgm/MidPlaneTool.hpp
  cgm/TBOwner.hpp
  cgm/TBOwnerSet.hpp
  cgm/TDCompare.hpp
  # Generated files:
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMConfigure.h"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMGeomConfigure.h")

set(CGM_GEOM_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
configure_file(
  "${CMAKE_SOURCE_DIR}/config/CGMGeomConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMGeomConfigure.h"
  @ONLY)

configure_file(
  "${CMAKE_SOURCE_DIR}/config/CGMConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMConfigure.h"
  @ONLY)

add_subdirectory(facet)
add_subdirectory(virtual)
add_subdirectory(Cholla)
add_subdirectory(facetbool)

if (CGM_HAVE_OCC)
  add_subdirectory(OCC)
endif ()

if (CGM_HAVE_MPI)
  add_subdirectory(parallel)
endif ()

# Now algorithmically generate header names
# from implementation filenames:
foreach (src IN LISTS geom_srcs)
  string(REGEX REPLACE ".cpp" ".hpp" header "${src}")
  set(header "cgm/${header}")
  list(APPEND geom_headers
    "${header}")
endforeach ()

set(cgm_geom_target cgm_geom PARENT_SCOPE)
set(cgm_geom_target cgm_geom)
cgm_add_library(cgm_geom 0  
  ${geom_srcs}
  ${extra_geom_srcs}
  )

if(CGM_LIBRARY_PROPERTIES)
  set_target_properties(cgm_geom
    PROPERTIES ${CGM_LIBRARY_PROPERTIES})
endif()

target_link_libraries(cgm_geom
  PUBLIC
    cgm_util
)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/cgm
    ${CMAKE_CURRENT_BINARY_DIR}/cgm
    ${CMAKE_SOURCE_DIR}/src/util/cgm
    ${CMAKE_BINARY_DIR}/src/util/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/virtual/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/facet/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/OCC/cgm
)

cgm_install_headers(${geom_headers})

